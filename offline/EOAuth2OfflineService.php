<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 09.04.14
 * @version    $Id: $
 */
require_once 'ISocialOffline.php';
require_once 'EOAuth2Service.php';
class EOAuth2OfflineService extends EOAuth2Service implements ISocialOffline {
    /**
     * Багфикс родительсокго компонента
     * @param EAuth $component
     * @param array $options
     */
    public function init($component, $options = array()) {
        if (isset($component)) {
            $this->setComponent($component);
        }
        foreach ($options as $key => $val) {
            $this->$key = $val;
        }
        if ('cli' !=php_sapi_name()){
            $this->setRedirectUrl(Yii::app()->user->returnUrl);
            $server = Yii::app()->request->getHostInfo();
            $path = Yii::app()->request->getPathInfo();
            $this->setCancelUrl($server . '/' . $path);
            $this->restoreAccessToken();
        }

    }
    /**
     * Получить токен авторизации
     * @return NULL|string
     */
    public function getToken(){
        return ($this->access_token!= NULL)?$this->access_token:parent::getState('auth_token');
    }

    /**
     * Получить время затухания
     * @return int
     */
    public function getTokenExpiration(){
        return $this->getState('expires');
    }

    /**
     * Установить токен
     * @param $token
     */
    public function setToken($token){
        $this->authenticated = true;
        $this->access_token = $token;
    }
}