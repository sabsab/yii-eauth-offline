<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 09.04.14
 * @version    $Id: $
 */
require_once 'ISocialOffline.php';
require_once 'EOAuthService.php';
 class EOAuthOfflineService extends EOAuthService implements ISocialOffline {
     /**
      * Багфикс родительсокго компонента
      * @param EAuth $component
      * @param array $options
      */

     public function init($component, $options = array()) {
         if (isset($component)) {
             $this->setComponent($component);
         }
         foreach ($options as $key => $val) {
             $this->$key = $val;
         }
         $this->auth = new EOAuthUserIdentity(array(
             'scope' => $this->scope,
             'key' => $this->key,
             'secret' => $this->secret,
             'provider' => $this->providerOptions,
         ));
         if ('cli' !=php_sapi_name()){
             $this->setRedirectUrl(Yii::app()->user->returnUrl);
             $server = Yii::app()->request->getHostInfo();
             $path = Yii::app()->request->getPathInfo();
             $this->setCancelUrl($server . '/' . $path);
             // Try to restore access token and customer from session.
             $this->restoreCredentials();
         }



     }
     /**
      * Получить токен авторизации
      * @param bool $asJson
      * @return NULL|string
      */
     public function getToken($asJson=true){
         /**
          * @var $token OAuthToken
          */
         $token = $this->getState('auth_token');
         if ($asJson){
             $token  = json_encode (
                 array(
                     "key"=>$token->key,
                     "secret"=>$token->secret
                 )
             );
         }
         return $token;

     }

     /**
      * Получить время затухания
      * @return int
      */
     public function getTokenExpiration(){
         return $this->getState('expires');
     }

     /**
      * Установить токен
      * @param $token
      */
     public function setToken($tokenString){

         $this->authenticated = true;
         $token_data  = json_decode ($tokenString);
         $token = new OAuthToken($token_data->key, $token_data->secret);

         $consumer = new OAuthConsumer($this->key, $this->secret);

         $provider = $this->auth->getProvider();

         $provider->consumer = $consumer;
         $provider->token = $token;

     }
}