<?php
/**
 * Соц сети позволяющие вытянуть профиль
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 09.04.14
 * @version    $Id: $
 */
interface IProfile {
    /**
     * Получить профиль
     * @return SocialProfile
     */
    public function getProfile();
}