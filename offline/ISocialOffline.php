<?php
/**
 * Интерфейс работы с offline соц сетями
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 01.04.14
 * @version    $Id: $
 */
interface ISocialOffline {
    public function setToken($token);
    public function getToken();
    public function getTokenExpiration();

}