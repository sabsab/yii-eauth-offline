<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 02.04.14
 * @version    $Id: $
 */
class SocialProfile {
    public $birthday;
    public $location;
    public $summary;
    public $specialties;
    public $work;
    public $languages;
    public $educations;
    public $skills;
    public $publications;
    public $volunteer;
    public $honors_awards;
    public $courses;
    public $patents;
    public $certifications;
    public $gender;
    public function __construct($params){
        foreach ($params as $name=>$param){
            $method = "set" . ucfirst ($name);
            if (method_exists($this, $method)){
                $this->$method($param);
            }

        }
    }

    /**
     * @param mixed $birthday
     */
    protected function setBirthday(DateTime $birthday){

        $this->birthday = $birthday;
        return $this;
    }

    /**
     * @param mixed $certifications
     */
    protected function setCertifications($certifications)
    {
        $this->certifications = $certifications;
        return $this;
    }

    /**
     * @param mixed $courses
     */
    protected function setCourses($courses)
    {
        $this->courses = $courses;
        return $this;
    }

    /**
     * @param mixed $educations
     */
    protected function setEducations($educations)
    {
        $this->educations = $educations;
        return $this;
    }

    /**
     * @param mixed $honors_awards
     */
    protected function setHonorsAwards($honors_awards)
    {
        $this->honors_awards = $honors_awards;
        return $this;
    }

    /**
     * @param mixed $languages
     */
    protected function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @param mixed $location
     */
    protected function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * @param mixed $patents
     */
    protected function setPatents($patents)
    {
        $this->patents = $patents;
        return $this;
    }

    /**
     * @param mixed $work
     */
    protected function setWork($work)
    {
        $this->work = $work;
        return $this;
    }

    /**
     * @param mixed $publications
     */
    protected function setPublications($publications)
    {
        $this->publications = $publications;
        return $this;
    }

    /**
     * @param mixed $skills
     */
    protected function setSkills($skills)
    {
        $this->skills = $skills;
        return $this;
    }

    /**
     * @param mixed $specialties
     */
    protected function setSpecialties($specialties)
    {
        $this->specialties = $specialties;
        return $this;
    }

    /**
     * @param mixed $summary
     */
    protected function setSummary($summary)
    {
        $this->summary = $summary;
        return $this;
    }

    /**
     * @param mixed $volunteer
     */
    protected function setVolunteer($volunteer)
    {
        $this->volunteer = $volunteer;
        return $this;
    }

    /**
     * @param mixed $gender
     */
    protected function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }


}