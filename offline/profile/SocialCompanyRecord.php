<?php
/**
 * Компания
 * Class SocialCompanyRecord
 */
class SocialCompanyRecord extends SocialProfileChunks{
    public $name;
    public $industry;
    public function __construct ($name, $industry=null) {
        $this->name = $name;
        $this->industry = $industry;
    }
}