<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 09.04.14
 * @version    $Id: $
 */
class SocialEducationRecord extends SocialProfileChunks{
    public $name;
    public $startYear;
    public $finishYear;
    public $type;
    public function __construct ($name, $startYear=null, $finishYear=null, $type=null){
        $this->name = $name;
        $this->startYear=$startYear;
        $this->finishYear=$finishYear;
        $this->type=$type;
    }
}