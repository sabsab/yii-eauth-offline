<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 09.04.14
 * @version    $Id: $
 */

class SocialGenderRecord extends SocialProfileChunks{
    public $gender;
    const MALE_GENDER="male";
    const FEMALE_GENDER="female";
    const NOSEX_GENDER="nosex";
    public function __construct ($gender=self::NOSEX_GENDER){
        $this->gender = $gender;
    }
}