<?php

/**
 * Владение языком
 * Class SocialLanguageRecord
 */
class SocialLanguageRecord extends SocialProfileChunks{
    public $name;
    public $level;
    public function __construct ($name, $level=null){
        $this->name = $name;
        $this->level=$level;
    }
}