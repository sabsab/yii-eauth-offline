<?php
/**
 * Местоположение
 * Class SocialLocationRecord
 */
class SocialLocationRecord extends SocialProfileChunks{
    public $country;
    public $city;
    public function __construct ($city, $country=null){
        $this->city = $city;
        $this->country=$country;
    }
}