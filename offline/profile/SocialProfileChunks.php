<?php
/**
 * базовая запись о чем-то
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 07.04.14
 * @version    $Id: $
 */
class SocialProfileChunks{
    public function toArray(){
        $result = array();
        foreach ($this as $key=>$value){
            $result[$key]=$value;
        }
        return $result;
    }
}
