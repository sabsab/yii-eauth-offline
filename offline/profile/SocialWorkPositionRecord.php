<?php
/**
 * Позиция на работе
 * Class SocialWorkPositionRecord
 */
class SocialWorkPositionRecord extends SocialProfileChunks{
    public $name;
    public $company;
    public function __construct (SocialCompanyRecord $company, $name){
        $this->company = $company;
        $this->name = $name;
    }
}