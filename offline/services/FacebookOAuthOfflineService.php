<?php
/**
 * Адаптер работы с facebook
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 05.02.14
 * @version    $Id: $
 */
class FacebookOAuthOfflineService extends FacebookOAuthService implements IProfile{
    /**
     * https://developers.facebook.com/docs/authentication/permissions/
     */
    protected $scope = 'email,publish_actions,user_birthday,user_education_history,user_interests,user_location,user_work_history';

    /**
     * http://developers.facebook.com/docs/reference/api/user/
     *
     * @see FacebookOAuthService::fetchAttributes()
     */
    protected function fetchAttributes() {
        $info = (object)$this->makeSignedRequest('https://graph.facebook.com/v2.8/me', array(
            'query' => array(
                'fields' => join(',', array(
                    'id',
                    'email',
                    'name',
                    'link',
                ))
            )
        ));


        $this->attributes['id'] = $info->id;
        $this->attributes['name'] = $info->name;
        if (!empty($info->link)){
            $this->attributes['url'] = $info->link;
        }

        $this->attributes['email'] = $info->email;
        $this->attributes['avatar'] = "https://graph.facebook.com/".$info->id."/picture?type=large";
    }


    /**
     * Получить профиль
     * @return SocialProfile
     */
    public function getProfile(){
        $info = $this->getAttributes();
        if (isset($info["birthday"])){
            $date = DateTime::createFromFormat("d/m/Y" ,$info["birthday"]);
            if (false !== $date){
                $info["birthday"] = $date;
            }

        }

        if (isset($info["work"])){
            //positions
            $works = array();
            foreach ($info["work"] as $work){
                if (isset($work->position) && isset($work->employer)){

                    $company = new SocialCompanyRecord($work->employer->name);

                    $pos = new SocialWorkPositionRecord($company, $work->position->name);
                    $works[] = $pos;
                }


            }
            $info["positions"] = $works;
            unset ($info["work"]);


        }
        if (isset($info["location"])){
            $parts = explode(",", $info["location"]->name);
            $location = NULL;
            if (count($parts)>1){
                $location = new SocialLocationRecord($parts[1], $parts[0]);
            }
            else {
                $location = new SocialLocationRecord($parts[0]);

            }
            if (!is_null($location)){
                $info["location"] = $location;
            }

        }
        if (isset($info["education"])){
            $educations=array();

            foreach ($info["education"] as $record){
                if (isset($record->school) && isset($record->school->name)){
                    $education = new SocialEducationRecord(
                        $record->school->name,
                        null, // не отдает сволочь
                        (isset($record->year) && isset($record->year->name))?$record->year->name:NULL,
                        isset($record->type)?$record->type:NULL
                    );
                    $educations[] = $education;
                }

            }
            $info["educations"] =$educations;
            unset ($info["education"]);

        }
        if (isset($info["bio"])){
            $info["summary"] =  $info["bio"];
            unset($info["bio"]);
        }
        $profile = new SocialProfile($info);
        return $profile;

    }

}