<?php
/**
 * Авторизация через linkedin
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 28.03.14
 * @version    $Id: $
 */

class LinkedInOAuthOfflineService extends LinkedinOAuthService implements ISocialOffline, IProfile{
    protected $scope ="r_basicprofile r_fullprofile r_emailaddress";
    protected $login_fields = array(
        "id",
        "first-name",
        "last-name",
        "picture-url",
        "email-address"
    );
    protected $profile_fields = array(
        "location",
        "date-of-birth",
        "summary",
        "specialties",
        "positions",
        "languages",
        "educations",
       // "three-current-positions",
        "skills",
        "publications",
        "volunteer",
        "honors-awards",
        "courses",
        "patents",
        "certifications"

    );

    /**
     * Минимальный набор для регистрации
     */
    protected function fetchAttributes() {

        $info = $this->getData($this->login_fields);

        $this->attributes['id'] = $info['id'];
        $this->attributes['first_name'] = $info['first-name'];
        $this->attributes['last_name'] = $info['last-name'];
        $this->attributes['avatar'] =  (isset($info['picture-url']))?$info['picture-url']:"";
        $this->attributes['email'] =  (isset($info['email-address']))?$info['email-address']:"";
    }

    /**
     * Выполнить запрос
     * @param $fields
     * @return array
     */
    protected function getData($fields){
        $string = implode (",", $fields);
        $info = $this->makeSignedRequest(
            "http://api.linkedin.com/v1/people/~:($string)",
            array(),
            false
        ); // json format not working :(

        return $this->parseInfo($info);
    }




    /**
     * Владения языками
     * @param $lang
     * @return SocialLanguageRecord
     */
    protected function getLanguage($lang){
        return  new SocialLanguageRecord((string) $lang->language->name);
    }

    /**
     * Место работы
     * @param $position
     * @return SocialWorkPositionRecord
     */
    protected function getPosition($position){
        $company = new SocialCompanyRecord($position["company"]["name"], $position["company"]["industry"]);
        $pos = new SocialWorkPositionRecord($company, (string) $position["title"]);

        return $pos;
    }

    /**
     * Навыки
     * @param $skill
     * @return SocialSkillRecord
     */
    protected function getSkill($skill){
        return new SocialSkillRecord ((string) $skill["skill"]["name"]);
    }

    /**
     * Университет/учебное заведение
     * @param $education
     * @return SocialEducationRecord
     */
    protected function getEducation($education){

        $educ = new SocialEducationRecord(
            $education["school-name"],
            isset($education["start-date"]["year"])?$education["start-date"]["year"]:null,
            isset($education["end-date"]["year"])?$education["end-date"]["year"]:null
        );

        return $educ;
    }

    /**
     * Все остальные объекты
     * @param $object
     * @return SocialProfileChunks
     */
    protected function getDefaultFormat($object){
        $result = new SocialProfileChunks();
        foreach  ($object as $key=>$value){
            $result->$key = $value;
        }


        return $result;
    }

    /**
     * Преобразование стнадратного ответа от API
     * @param $source
     * @param $attribute
     * @return array
     */
    protected function formatEntity($source, $attribute ){
        $result = array();
        $sourceKey = $attribute . "s";
        $callback = "get" . ucfirst($attribute);

        $attributes = $source[$sourceKey]["@attributes"];
        if ($attributes["total"]>1){
            foreach ($source[$sourceKey][$attribute] as $record){
                if (method_exists($this, $callback)){
                    $result[] = $this->$callback($record);
                }
                else {
                    $result[] = $this->getDefaultFormat($record);
                }

            }
        }
        elseif (1 == $attributes["total"] ){
            if (method_exists($this, $callback)){
                $result[] = $this->$callback($source[$sourceKey][$attribute]);
            }
            else {
                $result[] = $this->getDefaultFormat($source[$sourceKey][$attribute]);
            }
        }
        return $result;
    }

    /**
     * Получить проциль
     * @return SocialProfile
     */
    public function getProfile(){

        $fields = $this->profile_fields;
        $info = $this->getData($fields);
        if (isset($info["date-of-birth"])){
            $birthday = new DateTime($info["date-of-birth"]["year"] . "-" . $info["date-of-birth"]["month"]."-" . $info["date-of-birth"]["day"]);

            if (false !== $birthday){
                $info["birthday"] = $birthday;
            }

            unset($info["date-of-birth"]);
        }
        if (isset($info["location"])){
            $location = new SocialLocationRecord(null, $info["location"]["name"]);
            $info["location"] = $location;
        }

        if (isset($info["languages"])){
            $info["languages"] = $this->formatEntity($info, "language" );
        }
        if (isset($info["positions"])){
            $info["work"] = $this->formatEntity($info, "position");
            unset ($info["positions"]);
        }
        if (isset($info["skills"])){
            $info["skills"] = $this->formatEntity($info, "skill");
        }
        if (isset($info["educations"])){
            $info["educations"] = $this->formatEntity($info, "education");
        }
        $another_fields = array(
            "specialties",
            "publications",
            "volunteer",
            "honors-awards",
            "courses",
            "patents",
            "certifications"
        );
        foreach ($another_fields as $name){
            if (isset($info[$name])){
                $info["$name"] = $this->formatEntity($info, substr($name, 0 ,strlen($name)-1));
            }
        }

        $profile = new SocialProfile($info);
        return $profile;

    }
}