<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 06.02.14
 * @version    $Id: $
 */
class TwitterOAuthOfflineService  extends TwitterOAuthService{

    protected function fetchAttributes() {
        $info = $this->makeSignedRequest('https://api.twitter.com/1.1/account/verify_credentials.json');
        $this->attributes = (array) $info;

        $this->attributes['id'] = $info->id;
        $this->attributes['first_name'] = $info->name;
        $this->attributes['avatar'] = $info->profile_image_url;
        $this->attributes['last_name']  = "";

    }
}