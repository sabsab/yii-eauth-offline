<?php
/**
 * 
 * @category  
 * @package   
 * @subpackage 
 * @author: u.lebedev
 * @date: 06.02.14
 * @version    $Id: $
 */

class VkontakteOAuthOfflineService extends VKontakteOAuthService implements ISocialOffline, IProfile{

    // protected $scope = 'friends';

    protected function fetchAttributes() {
        $info = (array)$this->makeSignedRequest('https://api.vk.com/method/users.get.json', array(
                'query' => array(
                    'uids' => $this->uid,
                    'fields' => 'nickname, photo_medium, sex, bdate, city, country, education, screen_name', // uid, first_name and last_name is always available
                    //'fields' => 'nickname, sex, bdate, city, country, timezone, photo, photo_medium, photo_big, photo_rec',
                ),
            ));

        $info = $info['response'][0];

        $this->attributes = (array) $info;
        $this->attributes['id'] = $info->uid;
        $this->attributes['first_name'] = $info->first_name ;
        $this->attributes['last_name'] = $info->last_name;
        $this->attributes['url'] = 'http://vk.com/id' . $info->uid;

        if (!empty($info->screen_name)) {
            $this->attributes['username'] = $info->screen_name;
            $this->attributes['url'] = 'http://vk.com/' . $info->screen_name;
        }
        else {
            $this->attributes['username'] = 'id' . $info->uid;
        }

        $this->attributes['avatar'] = $info->photo_medium;
        if ($this->hasState("email")){
            $this->attributes['email'] = $this->getState("email");
        }

    }


    /**
     * Получить пол человека
     * @param $code
     * @return string
     */
    protected function getGenderByVkCode($code){
        $result=SocialGenderRecord::NOSEX_GENDER;

        if (1 == $code){
            $result=SocialGenderRecord::FEMALE_GENDER;
        }
        elseif (2 == $code){
            $result=SocialGenderRecord::MALE_GENDER;
        }

        return $result;
    }

    /**
     * Получить страну  по ID
     * @param $code
     * @return string
     */
    protected function getCountryByVkCode($countryId){
        $info = (array)$this->makeSignedRequest('https://api.vk.com/method/places.getCountryById', array(
            'query' => array(
                'cids' => $countryId
                ),
            )
        );
        if (!isset($info['response']) && !isset($info['response'][0])){
            throw new Exception("Invalid response");
        }

        $info = $info['response'][0];

        return $info->name;
    }

    /**
     * Получить город по ID города
     * @param $cityId
     * @return int
     */
    protected function getCityByVkCode($cityId){
        $info = (array)$this->makeSignedRequest('https://api.vk.com/method/places.getCityById', array(
            'query' => array(
                'cids' => $cityId
                ),
            )
        );
        if (!isset($info['response']) && !isset($info['response'][0])){
            throw new Exception("Invalid response");
        }

        $info = $info['response'][0];

        return $info->name;
    }
    public function getProfile(){

        $fields = $this->getAttributes();
        if (isset($fields["bdate"])){
            $date = DateTime::createFromFormat("d.m.Y" ,$fields["bdate"]);
            if (false !== $date){
                $fields["birthday"] = $date;
            }

            unset($fields["bdate"]);
        }
        if (isset($fields["sex"])){
            $fields["gender"]=$this->getGenderByVkCode($fields["sex"]);
            unset ($fields["sex"]);
        }
        else {
            $fields["gender"]=SocialGenderRecord::NOSEX_GENDER;
        }
        if (isset($fields["country"])){
            $country = $this->getCountryByVkCode($fields["country"]);
            $city = null;
            if (isset($fields["city"])){
                $city = $this->getCityByVkCode($fields["city"]);
            }
            $location = new SocialLocationRecord($city, $country);
            $fields["location"] = $location;
            unset ($fields["country"]);
            unset ($fields["city"]);
        }
        if (isset($fields["university_name"])){
            $education = new SocialEducationRecord(
                $fields["university_name"]
            );
            $fields["educations"] = $education;
        }

        $profile = new SocialProfile($fields);
        return $profile;

    }
}